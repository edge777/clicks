import { GraphQLResolveInfo } from 'graphql';
import { IResolvers } from 'graphql-tools';
import { Click } from "./types";

import * as fs from 'fs';
const clicks = require("./assets/clicks");

const resolver: IResolvers = {
  Query: {
    eligibleClicks(_: void, args:{ }, info: GraphQLResolveInfo) {
      return eligibleClicks();
    },
  },
  Mutation: {
    generateResultClicksJsonFile(_: void, args:{ }, info: GraphQLResolveInfo) {
      return generateEligibleClicksJSON();
    },
  },
};

function eligibleClicks(): Click[] {
  const clicksToBeAssessed = removeAllIneligibleIPs();
  return getEligibleClicks(clicksToBeAssessed);
}

function generateEligibleClicksJSON(): string {
  const result = eligibleClicks()
  fs.writeFile("src/assets/result-set.json", JSON.stringify(result), (err: any) => {   
    // Checking for errors
    if (err) throw err; 
    console.log("File<result-set.json> Generated!") 
  });
  return "File Generated: src/assets/result-set.json";
}

/**
 * Condition 3: If there are more than 10 clicks for an IP in the overall array of clicks, do not include any of those clicks in the result set. 
 */ 
function removeAllIneligibleIPs() {
  const inEligible: any = [];
  let IPs: string[] = clicks.map(function(click: Click){ return click.ip });
  const counts: any = {}
  for (const el of IPs) {
    counts[el] = counts[el] ? counts[el] + 1 : 1;
  }
  const uniqueIPs = new Set(IPs);
  uniqueIPs.forEach((el: any) => { if(counts[el] > 10) inEligible.push(el)});
  return clicks.filter((el: any) => !inEligible.includes(el.ip))
}

function getEligibleClicks(clicksToBeAssessed: any): Click[] {
  const resultArray = [];
  let startsWith = '';
  for(let i = 0; i< 24; i++) {
    const currentPeriod = clicksToBeAssessed.filter((el: any) => {
      if(i < 10) { startsWith = '0' + i} else startsWith = i.toString();
      return el.timestamp.split(' ')[1].startsWith(startsWith)
    })
    if(currentPeriod.length !== 0) {
      resultArray.push(getMostExpensiveClickForPeriod(currentPeriod));
    }
  }
  return resultArray;
}

function getMostExpensiveClickForPeriod(clicksInCurrentPeriod: Click[]): Click {
  let eligible: Click = clicksInCurrentPeriod[0];
  clicksInCurrentPeriod.forEach((click: Click) => {
    /**
     * Condition 1: For each IP within each one hour period, only the most expensive click is placed into the result set.
     */ 
    if(eligible.amount < click.amount) { 
      eligible = click; 
    } else if(eligible.amount === click.amount) 
    /**
     * Condition 2: If more than one click from the same IP ties for the most expensive click in a one hour period, only place the earliest click into the result set.
     */
    { 
      if(new Date(eligible.timestamp) < new Date(eligible.timestamp) ) {
        eligible = click;
      }
    }
  })
  return eligible;
}

export default resolver;
