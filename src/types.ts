export type Click = {
  ip: string,
  timestamp: string,
  amount: number
}