# Typescript-GraphQl- Clicks

## Application uses

- ExpressJS as the main framework
- Apollo GraphQL as the Middleware server to accept and serve GraphQL requests
- TypeScript as the Primary Language

## Folder
 - src/
   - assets
      * contains clicks.json(source) & result.set.json(result) file.
   - schema
      * contains GraphQL schema
   - resolver.ts // Contains Handlers for GraphQL calls to view or print Eligible clicks
   - schema.ts // Makes excecutable schema 
   - server.ts // Entry file
   - types.ts // Contains exported type for a Click

## Getting started
Application uses npm.
- npm install
- npm start
 - GraphQL playground could be accesses at: http://localhost:3000/graphql

## Details

There is a Query & a Mutation  

1. Query - eligibleClicks - Gets you the result as a respone to the call (Does not generates any file)

2. Mutation - generateResultClicksJsonFile - Writes the desired result of eligible Clicks to a file inside "assests" folder in JSON format. (The resultant file contains subset of original file clicks.json)

## Conditions
    1. For each IP within each one hour period, only the most expensive click is placed into the result set.
    2. If more than one click from the same IP ties for the most expensive click in a one hour period, only place the earliest click into the result set.
    3. If there are more than 10 clicks for an IP in the overall array of clicks, do not include any of those clicks in the result set. 